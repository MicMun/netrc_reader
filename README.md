# NetrcReader

**Linux only**

This program reads the first or the searched host from the $HOME/.netrc and outputs the user and the password on stdout.
You can use it to get this values easily into two bash variables.

The .netrc file format is originally from curl.

## Requirements

- autotools
- make
- C++-Compiler (tested with g++)

## Build and install from source

```bash

# Generate the autotools environment, the configure script and the Makefile.in
aclocal
autoconf
automake --add-missing

# Generate the Makefile and build and install the tool
./configure
make
make install
```

## Build and install from release

Gets the last release from [here](https://codeberg.org/MicMun/netrc_reader/releases).

```bash

# Unpack release
tar -xzvf netrc_reader-<VERSION>.tar.gz
cd netrc_reader-<VERSION>

# Generate the Makefile and build and install the tool
./configure
make
make install
```

## License

Copyright 2019 by MicMun

This program is free software: you can redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see
[http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).
