/* ========================================================================== */
/*  @file:     ncreader.cpp                                                   */
/*  @author:   MicMun                                                         */
/*  @version:  1.0, 27.08.2019                                                */
/* ========================================================================== */

#include "ncreader.hpp"

// constructor: new NCReader with an host (can be empty)
NCReader::NCReader(std::string host): host_(host) {
   readLoginInfos();
}

// getLoginInfo: Returns the wanted login info
LoginInfo NCReader::getLoginInfo() {
   if (host_.empty()) {
      return loginInfos_[0];
   } else {
      LoginInfo info{};

      for (LoginInfo li : loginInfos_) {
         if (li.machine == host_) {
            info = li;
            break;
         }
      }

      if (info.machine.empty()) {
         throw std::string{"Host {" + host_ + "} not found!"};
      }

      return info;
   }
}

// Returns the filename (with path) of the .netrc
static std::string getNetrcFile(std::string filename) {
   std::string homedir = getenv("HOME");
   return homedir + "/" + filename;
}

// readLoginInfos: reads the /home/$USER/.netrc in a vector
void NCReader::readLoginInfos() {
   const std::string NETRC = std::string {".netrc"};
   const std::string MACHINE = std::string {"machine "};
   const std::string LOGIN = std::string {"login "};
   const std::string PASSWORD = std::string {"password "};

   std::string filename = getNetrcFile(NETRC);
   std::ifstream file;
   file.open(filename);

   if (file.fail()) {
      throw std::string{"FEHLER: Datei {" + filename +
                        "} kann nicht geoeffnet werden!"};
   } else {
      std::string puffer;
      int count = 0;

      while (getline(file, puffer)) {
         if (puffer.rfind(MACHINE, 0) == 0) {
            loginInfos_.push_back(LoginInfo());
            loginInfos_[count].machine = puffer.substr(MACHINE.length());
            count++;
         } else if (puffer.rfind(LOGIN, 0) == 0) {
            loginInfos_[count-1].user = puffer.substr(LOGIN.length());
         } else if (puffer.rfind(PASSWORD, 0) == 0) {
            loginInfos_[count-1].passwd = puffer.substr(PASSWORD.length());
         }
      }

      if (file.eof()) {
         file.clear();
      }
      file.close();

      if (count == 0) {
         throw std::string{"FEHLER: Keine Hosts in{" + filename + "}!"};
      }
   }
}

/* =================================== EOF ================================== */
