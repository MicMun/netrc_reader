/* ========================================================================== */
/*  @file:     ncreader.hpp                                                   */
/*  @author:   MicMun                                                         */
/*  @version:  1.0, 27.08.2019                                                */
/* ========================================================================== */

#include "netrc_reader.hpp"
#include "logininfo.hpp"

#ifndef NCREADER_H
#define NCREADER_H

/* ========================================================================== */
/*  Class                                                                     */
/* ========================================================================== */

class NCReader {
   private:
      std::string host_;
      std::vector<LoginInfo> loginInfos_{};

      void readLoginInfos();
   public:
      NCReader(std::string host);
      LoginInfo getLoginInfo();
};

#endif

/* =================================== EOF ================================== */
