/* ========================================================================== */
/*  @file:     netrc_reader.cpp                                               */
/*  @author:   MicMun                                                         */
/*  @version:  1.0, 27.08.2019                                                */
/* ========================================================================== */

#include "ncreader.hpp"

static void printSyntax(const char *program) {
   std::cout << "Usage: " << program << " [HOST]...\n";
   std::cout << "Reads the user and password from $HOME/.netrc for ftp/http login.\n";
   std::cout << "\n";
   std::cout << "Arguments:\n"
             << " <HOST>       is the name of \"machine\" to search in the .netrc, without this argument the first one will be read."
             << "\n\n";
   std::cout << "Miscellaneous:\n" << " -h, --help   shows this help.\n";
}

// reads the arguments
static int get_args(int argc, char **argv, std::string& host) {
   int rc = OK;

   std::string argument{};

   if (argc < 2) {
      // weiter
   } else if (argc > 2) {
      printSyntax(argv[0]);
      rc = NOTOK;
   } else {
      argument = std::string{argv[1]};

      if (argument == "-h" || argument == "--help") {
         printSyntax(argv[0]);
         rc = 2;
      } else {
         host = argument;
      }
   }

   return rc;
}

// Parameter host: optional name of the host machine
int main(int argc, char **argv) {
   int rc = OK;

   std::string host;
   LoginInfo loginInfo {};

   if ((rc = get_args(argc, argv, host))) {
      if (rc == 2)
         rc = OK;
   } else {
      try {
         NCReader reader = NCReader(host);
         loginInfo = reader.getLoginInfo();
         std::cout << loginInfo.user << " " << loginInfo.passwd << "\n";
      } catch (std::string e) {
         std::cerr << e << "\n";
         rc = NOTOK;
      }
   }

   return rc;
}

/* =================================== EOF ================================== */
