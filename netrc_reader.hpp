/* ========================================================================== */
/*  @file:     netrc_reader.hpp                                               */
/*  @author:   MicMun                                                         */
/*  @version:  1.0, 27.08.2019                                                */
/* ========================================================================== */

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdlib>

#ifndef NETRC_READER_H
#define NETRC_READER_H

/* ========================================================================== */
/*  Defines                                                                   */
/* ========================================================================== */

/* Returncodes */
#ifndef OK
#define OK     (0)
#endif

#ifndef NOTOK
#define NOTOK  (1)
#endif

#endif

/* =================================== EOF ================================== */
